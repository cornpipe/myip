#!/usr/bin/env python3
'''
Title:  myip
Author:  Chris Skaggs
Description:  Simple script to show the user's external and internal ip addresses to the console
Usage:  $ python3 myip.py
          or
        move myip.py to $PATH location and remove .py extension (recommended)
        $ myip

Note: MUST have Python 3 installed, not compatible with Python 2.x
Changing this comment to test SoruceTree
'''

import urllib.request
import subprocess
import sys

def FindExternalIP():
    ipaddress = urllib.request.urlopen("http://icanhazip.com/").read()
    txt = str(ipaddress)
    for char in txt:
        if char in " b\\n'":
            txt = txt.replace(char, "")
    return txt

def FindInternalIP():
    ipaddress = subprocess.getoutput("ifconfig | grep \"inet addr\" | awk -F: '{print $2}' | awk '{print $1}'")
    ipaddress = ipaddress.replace("\n127.0.0.1", "")
    return ipaddress

def Help():
    print("Usage:  myip [option]")
    print("")
    print("Leave [option] blank to display both IP addresses")
    print(" -e  Returns just the external IP address")
    print(" -i  Returns just the internal IP address")
    print(" -h  Displays this help message")

if len(sys.argv) == 2:
    option = str(sys.argv[1])
    if option == "-e":
        print(FindExternalIP())
    elif option == "-i":
        print(FindInternalIP())
    else:
        Help()
elif len(sys.argv) > 2:
    Help()
else:
    print ("External:  " + FindExternalIP())
    print ("Internal:  " + FindInternalIP())
